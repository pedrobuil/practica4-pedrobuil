package clases;

public class ObraDeArte {
	private String codObra;
	private String nombre;
	private String autor;
	private String tipo;
	private int anoCreacion;
	private double precio;
	
	public ObraDeArte(String codObra, String nombre, String autor, String tipo, int anoCreacion, double precio) {
		this.codObra = codObra;
		this.nombre = nombre;
		this.autor = autor;
		this.tipo = tipo;
		this.anoCreacion = anoCreacion;
		this.precio = precio;
	}
	
	public ObraDeArte(String codObra) {
		this.codObra = codObra;
	}

	public String getcodObra() {
		return codObra;
	}
	
	public void setcodObra(String codObra) {
		this.codObra = codObra;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getAnoCreacion() {
		return anoCreacion;
	}

	public void setAnoCreacion(int anoCreacion) {
		this.anoCreacion = anoCreacion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	
	@Override
	public String toString() {
		return "ObraDeArte [codObra=" + codObra + ", nombre=" + nombre + ", autor=" + autor + ", tipo=" + tipo
				+ ", anoCreacion=" + anoCreacion + ", precio=" + precio + "]";
	}
}
